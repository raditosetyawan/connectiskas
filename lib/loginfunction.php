<?php
namespace dbase;
class loginfunction extends connfunction{

  	public function loginadm($user,$pass){
  		$user = $this->filter($user);
  		$pass = $this->filter($pass);

  		$q = "SELECT * FROM login WHERE user_name='$user'";

  		if($this->countQuery($q) > 0){

  			$tmp = $this->arrayQuery($q);
  			$pwd = $tmp['password'];
        $_SESSION['nama'] = $tmp['fullname'];

  			if(password_verify($pass,$pwd)){
  				$this->seslogin($user);
  				return true;
  			} else{
  				return false;
  			}
  		} else{
  			return false;
  		}
  	}
  	public function seslogin($usr){
  		$sesid = sha1(rand().rand());

  		$this->seset("sesid",$sesid);
  		$this->seset("uname",$usr);
  		$this->seset("loggedin","true");
  	}
  	public function isloggedin($true,$false){
  		if(isset($_SESSION['sesid']) && isset($_SESSION['loggedin'])){
  			//header("Location: ".$true);
  		} else {
  			header("Location: ".$false);
  			exit(0);
  		}
  	}
  	public function islogon(){
  		if(isset($_SESSION['sesid']) && isset($_SESSION['loggedin'])){
  			return true;
  		} else {
  			return false;
  		}
  	}
  	public function isloggedinFront($true,$false){
  		if(isset($_SESSION['sesid']) && isset($_SESSION['loggedin'])){
  			header("Location: ".$true);
  			exit(0);
  		} else {
  			//header("Location: ".$false);
  		}
  	}
  	public function movePage($page){
  		header("Location: ".$page);
  		exit(0);
  	}
  	public function frmValid($arr){
  		foreach($arr as $x){
  			if(!isset($_POST[$x])){
  				return false;
  			}
  		}
  		return true;
  	}
}
?>
