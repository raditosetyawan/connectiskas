<?php
namespace dbase;

class connfunction extends connection{
	protected function allQuery($query){
		$socy = $this->conn->query($query);
		return $socy -> fetch_all(1);
	}
	protected function arrayQuery($query){
		$socy = $this->conn->query($query);
		return $socy -> fetch_array(1);
	}
	protected function countQuery($query){
		$socy = $this->conn->query($query);
		$num = $socy->num_rows;
		return $num;
	}
	protected function justQuery($query){
		$socy = $this->conn->query($query);
		if($socy){
			return true;
		} else{
			return false;
		}
	}

	protected function filter($s){
		return $this->conn->real_escape_string($s);
	}
	protected function seset($name,$val){
		$_SESSION[$name] = $val;
	}

	public function get($param){
		return $_GET[$param];
	}
	public function post($param){
		return $_POST[$param];
	}
	public function rupiahs($angka){
		$hasil_rupiah = "Rp. " . number_format($angka,0,',','.');
		return $hasil_rupiah;
	}
	public function zeros($num,$d){
		$l = strlen($num);
		$tmp="";
		for($z=0;$z<($d-$l);$z++){
			$tmp = $tmp."0";
		}
		return $tmp.$num;
	}
	public function filled($data,$method){
		foreach($data as $z){
			if($method == "GET"){
				if(!isset($_GET[$z])){
					return false;
				} else{
					if($_GET[$z] == ""){
						return false;
					}
				}
			} elseif($method == "POST"){
				if(!isset($_POST[$z])){
					return false;
				} else{
					if($_POST[$z] == ""){
						return false;
					}
				}
			}
		}
		return true;
	}
	public function formatter($date){
		echo $date;
		$tmp = explode("/",$date);
		return $tmp[2]."-".$tmp[0]."-".$tmp[1];
	}
}
?>
