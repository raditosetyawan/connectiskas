<?php
namespace dbase;

class datafunction extends connfunction{
	public function loginadm($user,$pass){
		$user = $this->filter($user);
		$pass = $this->filter($pass);

		$q = "SELECT * FROM login WHERE user_name='$user' LIMIT 1";

		if($this->countQuery($q) > 0){

			$tmp = $this->arrayQuery($q);
			$pwd = $tmp['passwd'];

			if(password_verify($pass,$pwd)){
				$this->seslogin($user);
				return true;
			} else{
				return false;
			}
		} else{
			return false;
		}
	}
	public function seslogin($usr){
		$sesid = sha1(rand().rand());

		$this->seset("sesid",$sesid);
		$this->seset("uname",$usr);
		$this->seset("loggedin","true");
	}
	public function isloggedin($true,$false){
		if(isset($_SESSION['sesid']) && isset($_SESSION['loggedin'])){
			//header("Location: ".$true);
		} else {
			header("Location: ".$false);
			exit(0);
		}
	}
	public function islogon(){
		if(isset($_SESSION['sesid']) && isset($_SESSION['loggedin'])){
			return true;
		} else {
			return false;
		}
	}
	public function isloggedinFront($true,$false){
		if(isset($_SESSION['sesid']) && isset($_SESSION['loggedin'])){
			header("Location: ".$true);
			exit(0);
		} else {
			//header("Location: ".$false);
		}
	}
	public function movePage($page){
		header("Location: ".$page);
		exit(0);
	}
	public function frmValid($arr){
		foreach($arr as $x){
			if(!isset($_POST[$x])){
				return false;
			}
		}
		return true;
	}
	public function getSaldo(){
		$q = "SELECT saldo FROM iuran_kas ORDER BY no_transaksi DESC LIMIT 1";
		$x = $this->arrayQuery($q);
		$rp = $this->rupiahs($x['saldo']);
		return $rp;
	}
	public function getLastSaldo(){
		$q = "SELECT saldo FROM iuran_kas ORDER BY no_transaksi DESC LIMIT 1";
		$x = $this->arrayQuery($q);
		$rp = $x['saldo'];
		return $rp;
	}
	public function getJmlKas(){
		$q = "SELECT count(no_transaksi) as jml FROM iuran_kas ORDER BY no_transaksi DESC LIMIT 1";
		$x = $this->arrayQuery($q);
		return $x['jml'];
	}
	public function getDataKas(){
		//$q = $this->allQuery("SELECT * FROM iuran_kas WHERE substr(no_transaksi,1,4) >= 2018 ORDER BY no_transaksi ASC");
		$q = $this->allQuery("SELECT * FROM iuran_kas ORDER BY no_transaksi ASC");
		return $q;
	}
	public function getDataTbl(){
		$q = $this->allQuery("SELECT * FROM iuran_kas ORDER BY no_transaksi ASC");
		return $q;
	}
	public function getKasbyid($id){
		$id = $this->filter($id);
		$q = $this->allQuery("SELECT * FROM iuran_kas WHERE no_transaksi='$id' ORDER BY no_transaksi ASC");
		return $q;
	}

	public function getLastID(){
		date_default_timezone_set("Asia/Jakarta");

		$zo = $this->arrayQuery("SELECT no_transaksi FROM iuran_kas ORDER BY no_transaksi DESC LIMIT 1");

		$zo = $zo['no_transaksi'];
		$id = explode(".",$zo);

		$now = date("Ymd");

		if($now != $id[0]){
			$zo = $now.".001";
		} else {
			$zo = $id[0].".".$this->zeros(($id[1]+1),3);
		}

		return $zo;
	}

	public function inDebit($data){
		if(strpos($data['tgl'],"/")){
			$tgl = $this->filter($this->formatter($data['tgl']));
		} else{
			$tgl = $this->filter($data['tgl']);
		}
		$idx = $this->filter($data['id']);
		$ket = $this->filter($data['ket']);
		$jml = $this->filter((int) $data['jml']);

		$saldo = $this->getLastSaldo();
		$saldo +=$jml;

		$q = "INSERT INTO iuran_kas SET no_transaksi='$idx',tanggal='$tgl',keterangan='$ket',debit='$jml',kredit=0,saldo='$saldo'";

		return $this->justQuery($q);
	}
	public function inKredit($data){
		if(strpos($data['tgl'],"/")){
			$tgl = $this->filter($this->formatter($data['tgl']));
		} else{
			$tgl = $this->filter($data['tgl']);
		}
		$idx = $this->filter($data['id']);
		$ket = $this->filter($data['ket']);
		$jml = $this->filter((int) $data['jml']);

		$saldo = $this->getLastSaldo();
		$saldo -=$jml;

		$q = "INSERT INTO iuran_kas SET no_transaksi='$idx',tanggal='$tgl',keterangan='$ket',debit=0,kredit='$jml',saldo='$saldo'";

		return $this->justQuery($q);
	}
	public function delete($id){
		$id = $this->filter($id);
		$q = "DELETE FROM iuran_kas WHERE no_transaksi = '$id'";

		if($this->justQuery($q)){
			return true;
		} else{
			return false;
		}

	}
	public function updatedata(){
		$tmp = $this->getDataTbl();
		$no = 1;
		$saldox=0;

		foreach($tmp as $z){
			if($no == 1){
				$tmp = $z['debit'] - $z['kredit'];
				$saldox = $tmp;
			} else{
				$tmp = ($saldox-$z['kredit'])+$z['debit'];
				$saldox = $tmp;
			}

			$query = "UPDATE iuran_kas SET saldo = '$saldox' WHERE no_transaksi = '".$z['no_transaksi']."'";

			$this->justQuery($query);
			$no++;
		}
		return true;
	}
	public function editdata($data){
		$id = $data['id'];
		$tgl = $data['tanggal'];
		$debit = $data['debit'];
		$kredit = $data['kredit'];
		$ket = $data['ket'];

		$q = "UPDATE iuran_kas SET tanggal='$tgl',keterangan='$ket',debit='$debit',kredit='$kredit',saldo=0 WHERE no_transaksi='$id'";

		$exe = $this->justQuery($q);

		if($exe){
			$this->updatedata();
			return true;
		} else{
			return false;
		}
	}
}
?>
