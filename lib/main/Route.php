<?php
namespace main;

class Route{
  function __construct(){

  }
  public function get($param,$script){
    if(isset($_GET['page'])){
      if($_GET['page'] === $param){
        include $script;
      }
    }
  }

  public function post($param,$script){
    if(isset($_POST['page'])){
      if($_POST['page'] === $param){
        include $script;
      }
    }
  }

  public function check($param){
    if(isset($_GET['page'])){
      if($_GET['page'] == $param){
        return true;
      } else{
        return false;
      }
    } else{
      return false;
    }
  }
}
?>
