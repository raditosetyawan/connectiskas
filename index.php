<?php
session_start();
define('LOCAL',TRUE);

require "vendor/autoload.php";
use main\Route;
use dbase\loginfunction;

$x = new loginfunction();
$x->isloggedin("","login.php");

if($_GET['page'] == "" || !isset($_GET['page'])){
  header("Location: ?page=main");
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Kas Connectis | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/sweetalert/dist/sweetalert2x.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="dist/css/additional.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery-3.3.1.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>

<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="bower_components/sweetalert/dist/sweetalert2.all.min.js"></script>

<!-- <script src="https://unpkg.com/ionicons@4.2.5/dist/ionicons.js"></script>-->


<script>

function updtbl(){
  $.ajax({
    url: "API/api.tbl.php",
    type: "GET",
    success:function(res){
      var no =1;
      var tmp = $("#kastbl").DataTable();
      tmp.destroy();

      $("tbody").html("");
      for(z in res){
        $("tbody").append('<tr><td>'+no+'</td><td>'+res[z].no_transaksi+'</td><td>'+res[z].tanggal+'</td><td>'+res[z].keterangan+'</td><td>'+res[z].debit+'</td><td>'+res[z].kredit+'</td><td>'+res[z].saldo+'</td><td class="text-center"><a data-id="'+res[z].no_transaksi+'" class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></a> <a data-id="'+res[z].no_transaksi+'" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-trash"></i></a> </td></tr>');
        no++;
      }
      $('#kastbl').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false,
      }).page('last').draw('page');
    }
  });
}
function getsaldo(){
  $.ajax({
    url: "API/api.id.php",
    type: "GET",
    success:function(res){
      console.log(res);
      $("#saldos").html("<h4>Saldo <strong>"+res.saldo+"</strong></h4>");
      $("input[name=date]").val(moment().format('YYYY-MM-DD'));
    },
    error:function(erx){

    }
  });
}
$(function () {
  $('#kastbl').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  }).page('last').draw('page');

  $("h1").click(function(){
    updtbl();
  });
  $('input[name=date]').datepicker();
  $("input[name=date]").val(moment().format('YYYY-MM-DD'));
  function formtojson(data){
    let tmp = data.split("&");
    let datax = {};
    for(z in tmp){
      let x = tmp[z].split("=");
      datax[x[0]] = urldecode(x[1]);
    }
    return datax;
  }

  $.ajax({
    url: "API/api.id.php",
    type: 'GET',
    success:function(res){
      console.log(res);
      $("input[name='id']").val(res.lastid);
    },
    error:function(erx){

    }
  });

  $(".pull-right button").click(function(){
    $.ajax({
      url: "API/api.id.php",
      type: 'GET',
      success:function(res){
        console.log(res);
        $("input[name='id']").val(res.lastid);
      },
      error:function(erx){

      }
    });
  });
  $("#frm-kredit").submit(function(e){
    e.preventDefault();
    let data = $(this).serialize()+"&type=kredit";

    $.ajax({
      url: "API/api.action.php",
      type: "POST",
      data : data,

      success:function(res){
        $("#modal-kredit").modal('hide');
        $("#frm-kredit").trigger('reset');
        getsaldo();
        console.log(res);
        if(res.status == "ok"){
          setTimeout(function(){
            swal("Sukses!", "Data berhasil dimasukkan", "success");
          },100);
          updtbl();
        } else{
          swal("Error!", "Data gagal dimasukkan", "error");
        }
      },
      error:function(x,y,z){
        console.log(x,y,z);
        swal("Error!", "Data gagal dimasukkan", "error");
      }
    });

  });

  $("#frm-debit").submit(function(e){
    e.preventDefault();
    let data = $(this).serialize()+"&type=debit";

    $.ajax({
      url: "API/api.action.php",
      type: "POST",
      data : data,

      success:function(res){
        console.log(res);
        $("#modal-debit").modal('hide');
        $("#frm-debit").trigger('reset');
        getsaldo();
        if(res.status == "ok"){
          setTimeout(function(){
            swal("Sukses!", "Data berhasil dimasukkan", "success");
          },100);
          updtbl();
        } else{
          swal("Error!", "Data gagal dimasukkan", "error");
        }
      },
      error:function(x,y,z){
        console.log(x,y,z);
        swal("Error!", "Data gagal dimasukkan", "error");
      }
    });
  });

  $("#frm-edit").submit(function(e){
    e.preventDefault();
    let data = $(this).serialize();
    console.log(data);
    $.ajax({
      url: "API/api.upd.php",
      type: "POST",
      data : data,

      success:function(res){
        console.log(res);
        $("#modal-edit").modal('hide');
        $("#frm-edit").trigger('reset');
        getsaldo();
        if(res.error == "false"){
          setTimeout(function(){
            swal("Sukses!", "Data berhasil diupdate", "success");
          },100);
          updtbl();
        } else{
          swal("Error!", "Data gagal diupdate", "error");
        }
      },
      error:function(x,y,z){
        console.log(x,y,z);
        swal("Error!", "Data gagal dimasukkan", "error");
      }
    });
  });
  $("#edit-debit").keydown(function(){
    $("#edit-kredit").val("0");
  });
  $("#edit-kredit").keydown(function(){
    $("#edit-debit").val("0");
  });

})
$(document).on("click",".btn-edit",function(){
  let data = $(this).attr("data-id");

  $.ajax({
    url: "API/api.get.php",
    type: "POST",
    data: "id="+data,

    success:function(res){
      $("#edit-id").val(res[0].no_transaksi);
      $("#edit-ket").val(res[0].keterangan);
      $(".edit-tanggal").val(res[0].tanggal);
      $("#edit-debit").val(res[0].debit);
      $("#edit-kredit").val(res[0].kredit);
    },
    error:function(x,y,z){
      console.log(x,y,z);
    }
  });
  setTimeout(function(){
    $("#modal-edit").modal();
  },100);
});
$(document).on("click",".btn-delete",function(){
  var id =$(this).attr("data-id");
  console.log(id);
  const swalWithBootstrapButtons = swal.mixin({
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
  })

  swalWithBootstrapButtons({
    title: 'Apakah anda yakin?',
    text: "Data yang terhapus tidak dapat dikembalikan !",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: "API/api.delete.php",
        type: "POST",
        data: "id="+id,
        success:function(res){
          swalWithBootstrapButtons(
            'Terhapus!',
            'Data telah terhapus.',
            'success'
          )
          updtbl();
          getsaldo();
        },
        error:function(x,y,z){
          console.log(x,y,z);
        }
      });
    } else if (
      // Read more about handling dismissals
      result.dismiss === swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons(
        'Dibatalkan',
        'Operasi dibatalkan',
        'error'
      )
    }
  })
});
</script>

<script src="bower_components/morris.js/morris.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="?page=main" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>K</b>C</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Kas</b>Connectis</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="dist/img/avatar3.png" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $_SESSION['nama']; ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="dist/img/avatar3.png" class="img-circle" alt="User Image">
                  <h2 class="text-white">Hello <?php echo $_SESSION['nama']; ?></h2>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <!--
                  <div class="pull-left">
                      <a href="profile.php" class="btn btn-default btn-flat">Profile</a>
                  </div>
                -->
                  <div class="pull-right">
                    <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="dist/img/avatar3.png" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>Hello, <?php echo $_SESSION['nama']; ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MAIN NAVIGATION</li>
          <li class="<?php if(Route::check('main')) echo "active"; ?>">
            <a href="?page=main">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
          </li>
          <li class="<?php if(Route::check('jurnal')) echo "active"; ?>">
            <a href="?page=jurnal">
              <i class="fa fa-book"></i> <span>Jurnal Harian</span>
            </a>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <?php
      Route::get("main","page/main.php");
      Route::get("jurnal","page/jurnal.php");
      ?>

    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1
      </div>
      <strong>Copyright &copy; 2018 <a href="http://solonet.net.id">PT Solo Jala Buana</a>.</strong> All rights
      reserved.
    </footer>
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
</body>
</html>
