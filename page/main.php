<?php
if(!defined('LOCAL')){
  require "../vendor/autoload.php";
}

use main\Main;
use dbase\datafunction;

Main::directaccess();

$d = new datafunction();

$saldo = $d->getSaldo();
$trans = $d->getJmlKas();
?>
<script type="text/javascript">
</script>

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-6 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3><?php echo $trans; ?></h3>

            <p>Total Kas</p>
          </div>
          <div class="icon">
            <i class="fa fa-book"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-6 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3><?php echo $saldo; ?></h3>

            <p>Saldo Akhir</p>
          </div>
          <div class="icon">
            <i class="fa fa-dollar"></i>
          </div>
          <a href="?page=jurnal" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
      <!-- /.row -->
    </div>
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
