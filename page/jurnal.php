<?php
if(!defined('LOCAL')){
  require "../vendor/autoload.php";
}

use main\Main;
use dbase\datafunction;

Main::directaccess();

$d = new datafunction();

$saldo = $d->getSaldo();
$trans = $d->getJmlKas();

$tmpdata = $d->getDataKas();
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Jurnal Harian
    <small>Connectis</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Jurnal Harian</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12">
          <div class="pull-left" id="saldos">
            <h4>Saldo <strong><?php echo $saldo; ?></strong></h4>
          </div>
          <div class="pull-right">
            <button type="button" name="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#modal-debit">Debit</button>
            <button type="button" name="button" class="btn btn-success btn-flat" data-toggle="modal" data-target="#modal-kredit">Kredit</button>
          </div>
        </div>
      </div>
      <div class="box">
        <div class="box-body">
          <table id="kastbl" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>No Transaksi</th>
                <th>Tanggal</th>
                <th>Keterangan</th>
                <th>Debit</th>
                <th>Kredit</th>
                <th>Saldo</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $i=1;

              foreach($tmpdata as $tmp){

                echo '
                <tr>
                  <td>'.$i.'</td>
                  <td>'.$tmp['no_transaksi'].'</td>
                  <td>'.$tmp['tanggal'].'</td>
                  <td>'.$tmp['keterangan'].'</td>
                  <td>'.$tmp['debit'].'</td>
                  <td>'.$tmp['kredit'].'</td>
                  <td>'.$tmp['saldo'].'</td>
                  <td class="text-center"><a data-id="'.$tmp['no_transaksi'].'" class="btn btn-warning btn-xs btn-edit"><i class="fa fa-pencil"></i></a> <a data-id="'.$tmp['no_transaksi'].'" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-trash"></i></a> </td>
                </tr>';

                $i++;
              }
              ?>

            </tbody>
            <tfoot>

            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

<div class="modal fade" id="modal-debit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h3 class="modal-title">Kas Masuk</h3>
        </div>
        <div class="modal-body">
          <form class="" id="frm-debit" action="index.html" method="post">
            <div class="form-group">
              <label>ID</label>
              <input type="text" class="form-control" name="id" id="id" readonly>
            </div>
            <div class="form-group">
              <label>Tanggal</label>
              <input type="text" class="form-control" id="datepicker" name="date" placeholder="Tanggal" autocomplete="off">
            </div>

            <div class="form-group">
              <label>Keterangan</label>
              <input type="text" class="form-control" name="keterangan" placeholder="Keterangan" autocomplete="off">
            </div>

            <div class="form-group">
              <label>Jumlah</label>
              <input type="number" class="form-control" name="jumlah" placeholder="Jumlah" autocomplete="off">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary">Reset</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="modal-kredit">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Kas Keluar</h3>
          </div>
          <div class="modal-body">
            <form class="" id="frm-kredit" action="index.html" method="post">
              <div class="form-group">
                <label>ID</label>
                <input type="text" class="form-control" name="id" id="id" readonly>
              </div>
              <div class="form-group">
                <label>Tanggal</label>
                <input type="text" class="form-control" id="datepicker" name="date" placeholder="Tanggal" autocomplete="off">
              </div>

              <div class="form-group">
                <label>Keterangan</label>
                <input type="text" class="form-control" name="keterangan" placeholder="Keterangan" autocomplete="off">
              </div>

              <div class="form-group">
                <label>Jumlah</label>
                <input type="number" class="form-control" name="jumlah" placeholder="Jumlah" autocomplete="off">
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary">Reset</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->
  <!-- /.content -->

  <div class="modal fade" id="modal-edit">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Edit Kas</h3>
          </div>
          <div class="modal-body">
            <form class="" id="frm-edit" action="index.html" method="post">
              <div class="form-group">
                <label>ID</label>
                <input type="text" class="form-control" name="ide" id="edit-id" readonly>
              </div>
              <div class="form-group">
                <label>Tanggal</label>
                <input type="text" class="form-control edit-tanggal" id="datepicker" name="date" placeholder="Tanggal" autocomplete="off">
              </div>

              <div class="form-group">
                <label>Keterangan</label>
                <input type="text" class="form-control" name="keterangan" id="edit-ket" placeholder="Keterangan" autocomplete="off">
              </div>

              <div class="form-group">
                <label>Debit</label>
                <input type="number" class="form-control" id="edit-debit" name="debit" placeholder="0" autocomplete="off">
              </div>

              <div class="form-group">
                <label>Kredit</label>
                <input type="number" class="form-control" id="edit-kredit" name="kredit" placeholder="0" autocomplete="off">
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary">Reset</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->
  <!-- /.content -->
