<?php
session_start();
header("Content-type: text/json");
include "../vendor/autoload.php";
use dbase\datafunction;
use dbase\loginfunction;

$x = new loginfunction();
$x->isloggedin("","../login.php");
$z = new datafunction();

$param = [
  "id"
];

if($z->filled($param,"POST")){
  echo json_encode($z->getKasbyid($_POST['id']));
} else{
  echo json_encode([
    "error" => "true",
    "msg" => "parameter not filled!"
  ]);
}
?>
