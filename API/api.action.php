<?php
session_start();
header("Content-type: text/json");
include "../vendor/autoload.php";
use dbase\datafunction;
use dbase\loginfunction;

$x = new loginfunction();
$x->isloggedin("","../login.php");

$z = new datafunction();

$param = [
  "id","date","keterangan","jumlah","type"
];

if($z->filled($param,"POST")){
  $data = [
    "id" => $_POST['id'],
    "tgl" => $_POST['date'],
    "ket" => $_POST['keterangan'],
    "jml" => $_POST['jumlah']
  ];
  if($_POST['type'] == "kredit"){
    if($z->inKredit($data)){
      $json = [
        "status" => "ok",
        "error" => null,
        "post" => $_POST
      ];
    } else{
      $json = [
        "status" => "error",
        "error" => null
      ];
    }
  } elseif($_POST['type'] == "debit"){
    if($z->inDebit($data)){
      $json = [
        "status" => "ok",
        "error" => null,
        "post" => $_POST
      ];
    } else{
      $json = [
        "status" => "error",
        "error" => null
      ];
    }
  } else{
    $json = [
      "status" => "error",
      "error" => null
    ];
  }

} else{
  $json = [
    "status" => "error",
    "error" => "paremeter not filled !"
  ];
}

echo json_encode($json,JSON_PRETTY_PRINT);
?>
