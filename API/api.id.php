<?php
session_start();
header("Content-type: text/json");

require "../vendor/autoload.php";
use dbase\datafunction;
use dbase\loginfunction;

$x = new loginfunction();
$x->isloggedin("","../login.php");
$z = new datafunction();

echo json_encode([
  "status" => "ok",
  "lastid" => $z->getLastID(),
  "saldo" => $z->getSaldo()
], JSON_PRETTY_PRINT);

?>
