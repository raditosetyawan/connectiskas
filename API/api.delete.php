<?php
session_start();
header("Content-type: text/json");
include "../vendor/autoload.php";
use dbase\datafunction;
use dbase\loginfunction;

$x = new loginfunction();
$x->isloggedin("","../login.php");
$z = new datafunction();

$param = [
  "id"
];

if($z->filled($param,"POST")){
  $id = $_POST['id'];
  if($z->delete($id)){
    $z->updatedata();

    $json = [
        "status" => "ok",
        "msg" => "item deleted !"
      ];
  } else{
      $json = [
        "status" => "error",
        "error" => null
      ];
  }
} else{
  $json = [
    "status" => "error",
    "error" => "paremeter not filled !"
  ];
}

echo json_encode($json,JSON_PRETTY_PRINT);
?>
