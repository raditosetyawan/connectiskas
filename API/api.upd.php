<?php
session_start();
header("Content-type: text/json");
include "../vendor/autoload.php";
use dbase\datafunction;
use dbase\loginfunction;

$x = new loginfunction();
$x->isloggedin("","../login.php");
$z = new datafunction();

$param = [
  "ide","date","keterangan","debit","kredit"
];

if($z->filled($param,"POST")){
  if(strpos($_POST['date'],"/")){
    $date = $z->formatter($_POST['date']);
  } else{
    $date = $_POST['date'];
  }

  $exe = $z->editdata([
    "id" => $_POST['ide'],
    "debit" => $_POST['debit'],
    "tanggal" => $date,
    "kredit" => $_POST['kredit'],
    "ket" => $_POST['keterangan']
  ]);

  if($exe){
    $js = [
      "error" => "false",
      "msg" => "data sukses diupdate"
    ];
  } else{
    $js = [
      "error" => "true",
      "msg" => "data gagal"
    ];
  }
  echo json_encode($js);

} else{
  echo json_encode([
    "error" => "true",
    "msg" => "parameter not filled!"
  ]);
}
?>
